const express = require('express');
const http = require('http');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const port = parseInt(process.env.PORT);
const server_port = parseInt(process.env.SERVER_PORT);
console.log(`Got port ${port}!`);


app.use('/static', express.static(__dirname + '/static'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/weather/city', (req, res) => {
    http.get(`http://backend:${server_port}/weather/city?q=${req.query.q}&f=${req.query.f}`, (response) => {
        if (response.statusCode === 404) {
            res.status(404).send("City not found");
        } else {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                data = JSON.parse(data);
                res.status(200).send(data);
            })
        }
    }).on("error", (err) => {
        res.status(404).send("City not found");
    })
});

app.get('/weather/coordinates', (req, res) => {
    http.get(`http://backend:${server_port}/weather/coordinates?lon=${req.query.lon}&lat=${req.query.lat}`, (response) => {
        if (response.statusCode === 404) {
            res.status(404).send("City not found");
        } else {
            let data = '';
            response.on('data', (chunk) => {
                data += chunk;
            });
            response.on('end', () => {
                data = JSON.parse(data);
                res.status(200).send(data);
            })
        }
    }).on("error", (err) => {
        res.status(404).send("City not found");
    })
});

app.get('/favorites', (req, res) => {
    http.get(`http://backend:${server_port}/favorites`, (response) => {
        let data = '';
        response.on('data', (chunk) => {
            data += chunk;
        });
        response.on('end', () => {
            data = JSON.parse(data);
            res.status(200).send(data);
        })
    }).on("error", (err) => {
        res.status(404).send("City not found");
    })
})

app.delete('/favorites', (req, res) => {
	http.get(`http://backend:${server_port}/favorites_d?q=${req.query.q}`, (response) => {
		let data = '';
		response.on('data', (chunk) => {
			data += chunk;
		});
		response.on('end', () => {
			res.status(200).send("OK");
        });			
	})
})

app.listen(port, () => console.log(`Weather app listening on port ${port}!`));
