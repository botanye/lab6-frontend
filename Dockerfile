FROM weather_site_system:latest AS build
RUN mkdir /weather_site
WORKDIR /weather_site
RUN git clone https://gitlab.com/botanye/lab6-frontend.git ./
RUN npm install express
RUN npm install dotenv
RUN npm install -g pkg
RUN pkg . -o front
CMD cp /weather_site/front /volume/front
